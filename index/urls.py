from django.urls import path, re_path

from . import views

from .views import getItemListNews, getIndexCategory, patchRegister

app_name = "home"

urlpatterns = [
    
    # re_path(r'',views.media, name='media'),
    path('', views.indexViews.as_view(), name='index'),
    path('category/news/<int:pk>/',views.getItemListNews.as_view(),  name='detailID'),
    path('news/<slug:slug>', views.getItemListNews.as_view(),  name='detail'),
    path('category/<int:pk>/',getIndexCategory.as_view(), name='category'),
    path('register/', views.register, name="register"),
]