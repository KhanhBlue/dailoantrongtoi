from django.shortcuts import render
from django.http import HttpResponse
# from .views123.getCategory import Category
from index.models import Category, insertScript
from index.models import News, Slide
from index.models import logoOrBanner
from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm

# Create your views here.

def calculator_number_news(list_input, number_1, numbner_2):
        out_number_1 = 0
        out_number_2 = 0
        len_list = len(list_input)

        if len_list >= number_1:
            out_number_1 = number_1
        else:
            out_number_1 = len_list

        if len_list >= numbner_2:
            out_number_2 = numbner_2
        else:
            out_number_2 = len_list

        return out_number_1, out_number_2

from .forms import RegistrationForm
from django.http import HttpResponseRedirect 

def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    return render(request, 'register.html', {'form': form})

def common_load(context):
    context.update({'Logo': logoOrBanner.objects.filter(logoOrBanner='logo', is_active=True).first()})
    context.update({'Banner': logoOrBanner.objects.filter(logoOrBanner='banertop', is_active=True).first()})
    context.update({'logohead': logoOrBanner.objects.filter(logoOrBanner='logohead', is_active=True).first()})
    context.update({'insertScriptHeader': insertScript.objects.filter(position='header', is_active=True).first()})
    context.update({'insertScriptFooter': insertScript.objects.filter(position='footer', is_active=True).first()})
    news_daibac = 10
    news_daitrung = 11
    news_caohung_binhdong = 12
    news_dainam = 13
    news_holy_gvgd = 14
    news_duhoc_dailoan = 15

    context.update({'Menus': Category.objects.filter().order_by('position_menu')})
    listNews = News.objects.filter(is_news=True, is_active=True).order_by('-id')
    if listNews:
        number_1, number_2 = calculator_number_news(listNews,1,7)
        context.update({'newNews': listNews[0]})
        context.update({'newNews7': listNews[number_1:number_2]})
    else:
        context.update({'newNews': listNews})
        context.update({'newNews7': listNews})

    news_daibac = News.objects.filter(category__id=news_daibac, is_active=True).order_by('-id')
    if news_daibac:
        context.update({'news_daibac': news_daibac[0:1]})
    else:
        context.update({'news_daibac': news_daibac})

    news_daitrung = News.objects.filter(category__id=news_daitrung, is_active=True).order_by('-id')
    if news_daitrung:
        context.update({'news_daitrung': news_daitrung[0:1]})
    else:
        context.update({'news_daitrung': news_daitrung})

    news_caohung_binhdong = News.objects.filter(category__id=news_caohung_binhdong, is_active=True).order_by('-id')
    if news_caohung_binhdong:
        context.update({'news_caohung_binhdong': news_caohung_binhdong[0:1]})
    else:
        context.update({'news_caohung_binhdong': news_caohung_binhdong})

    news_dainam = News.objects.filter(category__id=news_dainam, is_active=True).order_by('-id')
    if news_dainam:
        context.update({'news_dainam': news_dainam[0:1]})
    else:
        context.update({'news_dainam': news_dainam})

    news_holy_gvgd = News.objects.filter(category__id=news_holy_gvgd, is_active=True).order_by('-id')
    if news_holy_gvgd:
        context.update({'news_holy_gvgd': news_holy_gvgd[0:1]})
    else:
        context.update({'news_holy_gvgd': news_holy_gvgd})

    news_duhoc_dailoan = News.objects.filter(category__id=news_duhoc_dailoan, is_active=True).order_by('-id')
    if news_duhoc_dailoan:
        context.update({'news_duhoc_dailoan': news_duhoc_dailoan[0:1]})
    else:
        context.update({'news_duhoc_dailoan': news_duhoc_dailoan})

    return context

def blog_post(post_id):
    #your code
    try:
        blog_object=News.objects.get(id=post_id)
        blog_object.views_number=blog_object.views_number+1
        blog_object.save()
        #your code
    except:
        pass



class indexViews(ListView):
    model = News
    template_name = 'home.html'

    news_taiwan = 1
    news_vietNam = 2
    news_world = 16
    news_travel = 3
    news_xkld = 5
    news_study_abroad = 4
    news_life_taiwan = 7
    news_community = 8



    def get_context_data(self, **kwargs):
        # print("tinh lại slug==== start")
        # abc = News.objects.filter()
        # for item in abc:
        #     item.save()
        # print("tinh lại slug==== end")
        context = super().get_context_data(**kwargs)
        common_load(context)

        images_slider = Slide.objects.filter(is_active=True).order_by('-id')

        context.update({'imagesSlider': images_slider})
        context.update({ 'news_taiwan': self.news_taiwan })
        context.update({ 'news_vietnam': self.news_vietNam })
        context.update({ 'news_travel': self.news_travel })
        context.update({ 'news_world': self.news_world })
        context.update({ 'news_xkld': self.news_xkld })
        context.update({ 'news_study_abroad': self.news_study_abroad })
        context.update({ 'news_life_taiwan': self.news_life_taiwan })
        context.update({ 'news_community': self.news_community })

        list_news_taiwan = News.objects.filter(category__id=self.news_taiwan, is_active=True).order_by('-id')
        if  list_news_taiwan:
            number_1, number_2 = calculator_number_news( list_news_taiwan,2,6)
            context.update({'NewsTaiWan2':  list_news_taiwan[0:number_1]})
            context.update({'NewsTaiWan3':  list_news_taiwan[number_1:number_2]})
        else:
            context.update({'NewsTaiWan2':  list_news_taiwan})
            context.update({'NewsTaiWan3':  list_news_taiwan})

        list_news_vietNam = News.objects.filter(category__id=self.news_vietNam, is_active=True).order_by('-id')
        if list_news_vietNam:
            number_1, number_2 = calculator_number_news(list_news_vietNam,2,6)
            context.update({'NewsVietNam2': list_news_vietNam[0:number_1]})
            context.update({'NewsVietNam3': list_news_vietNam[number_1:number_2]})
        else:
            context.update({'NewsVietNam2': list_news_vietNam})
            context.update({'NewsVietNam3': list_news_vietNam})

        list_news_world = News.objects.filter(category__id=self.news_world,is_active=True).order_by('-id')
        if list_news_world:
            number_1, number_2 = calculator_number_news(list_news_world,2,6)
            context.update({'NewsWorld2': list_news_world[0:number_1]})
            context.update({'NewsWorld3': list_news_world[number_1:number_2]})
        else:
            context.update({'NewsWorld2': list_news_world})
            context.update({'NewsWorld3': list_news_world})

        list_news_travel = News.objects.filter(category__id=self.news_travel,  is_active=True).order_by('-id')
        if list_news_travel:
            number_1, number_2 = calculator_number_news(list_news_travel,0,6)
            context.update({'NewsTravel6': list_news_travel[:number_2]})
        else:
            context.update({'NewsTravel6': list_news_travel})

        # list_news_xkld = News.objects.filter(category__id=self.news_xkld,is_active=True).order_by('-id')
        # if list_news_xkld:
        #     number_1, number_2 = calculator_number_news(list_news_xkld,0,3)
        #     context.update({'NewsXKLD3': list_news_xkld[:number_2]})
        # else:
        #     context.update({'NewsXKLD3': list_news_xkld})

        list_news_study_abroad = News.objects.filter(category__id=self.news_study_abroad,is_active=True).order_by('-id')
        if list_news_study_abroad:
            number_1, number_2 = calculator_number_news(list_news_study_abroad,2,6)
            context.update({'NewsStudyAbroad2': list_news_study_abroad[0:number_1]})
            context.update({'NewsStudyAbroad3': list_news_study_abroad[number_1:number_2]})
        else:
            context.update({'NewsStudyAbroad2': list_news_study_abroad})
            context.update({'NewsStudyAbroad3': list_news_study_abroad})

        
        list_news_life_taiwan = News.objects.filter(category__id=self.news_life_taiwan, is_active=True).order_by('-id')
        if list_news_life_taiwan:
            number_1, number_2 = calculator_number_news(list_news_life_taiwan,0,6)
            context.update({'NewsLifeTaiwan6': list_news_life_taiwan[:number_2]})
        else:
            context.update({'NewsLifeTaiwan6': list_news_life_taiwan})

        # list_news_community = News.objects.filter(category__id=self.news_community, is_active=True).order_by('-id')
        # if list_news_community:
        #     number_1, number_2 = calculator_number_news(list_news_community,0,3)
        #     context.update({'NewsCommunity6': list_news_community[:number_2]})
        # else:
        #     context.update({'NewsCommunity6': list_news_community})

        return context


class getItemListNews(DetailView):
    model = News
    template_name = 'news-single.html'

    def get_context_data(self, **kwargs):
        context = super(getItemListNews, self).get_context_data(**kwargs)
        common_load(context)
        try:
            blog_post(kwargs['object'].id)
            category_id = context['object'].category_id
        except:
            category_id = 1

        NewsRelateTo = News.objects.filter(category__id = category_id, is_active=True).order_by('-id')
        if NewsRelateTo:
            number_1, number_2 = calculator_number_news(NewsRelateTo,1,5)
            context.update({'NewsRelateTo': NewsRelateTo[:number_2]})
        else:
            context.update({'NewsRelateTo': NewsRelateTo})

        return context


class getIndexCategory(ListView):
    model = News
    paginate_by = 15
    template_name = 'news.html'

    def get_queryset(self):
        list_object1 = News.objects.filter(category__id=self.kwargs['pk'], is_active=True).order_by('-id')
        return list_object1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        common_load(context)
        return context

class patchRegister(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'register.html'

