# Generated by Django 3.0.3 on 2020-05-13 16:50

import ckeditor.fields
import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0024_scriptfacebook'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'Chuyên mục(Category)'},
        ),
        migrations.AlterModelOptions(
            name='logoorbanner',
            options={'verbose_name_plural': 'Logo và Banner'},
        ),
        migrations.AlterModelOptions(
            name='news',
            options={'verbose_name_plural': 'Bài viết(News)'},
        ),
        migrations.AlterModelOptions(
            name='slide',
            options={'verbose_name_plural': 'Slide(5 ảnh động)'},
        ),
        migrations.RemoveField(
            model_name='news',
            name='title_url',
        ),
        migrations.AddField(
            model_name='category',
            name='slug',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='news',
            name='slug',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='author',
            field=models.CharField(blank=True, help_text='Tác giả', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='avata',
            field=models.ImageField(blank=True, help_text='Ảnh đại diện.', null=True, upload_to='image/'),
        ),
        migrations.AlterField(
            model_name='category',
            name='create_at',
            field=models.DateTimeField(auto_now_add=True, help_text='Ngày tạo'),
        ),
        migrations.AlterField(
            model_name='category',
            name='description',
            field=models.CharField(help_text='Mô tả của tiêu đề', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='is_active',
            field=models.BooleanField(default=True, help_text='Còn hoạt động hay không.'),
        ),
        migrations.AlterField(
            model_name='category',
            name='is_category',
            field=models.BooleanField(default=False, help_text='Nếu là trang có nhiều bài viết vd: TIN ĐÀI LOAN không tích. Nếu là trang Liên Hệ sẽ tích'),
        ),
        migrations.AlterField(
            model_name='category',
            name='is_menu_parent',
            field=models.IntegerField(default=0, help_text="VD: Nếu là 'ĐƠN HÀNG ĐÀI TRUNG' sẽ điền là 0 - nếu là 'XKLĐ_ DU HỌC' điền 1 => Không có menu con sẽ điền là 0, có điền là 1", null=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='menu_parent',
            field=models.IntegerField(default=0, help_text="VD: Nếu là 'ĐƠN HÀNG ĐÀI TRUNG' sẽ có menu parent là 'XKLĐ_ DU HỌC'", null=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='position_menu',
            field=models.IntegerField(default=0, help_text='Vị trí menu hiển thị.', null=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(help_text='Tiêu đề', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='title_url',
            field=models.CharField(blank=True, default='', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='author',
            field=models.CharField(default='Tin Đài Loan', help_text='Người tạo', max_length=200),
        ),
        migrations.AlterField(
            model_name='news',
            name='avata',
            field=models.ImageField(help_text='Ảnh đại diện', null=True, upload_to='image/'),
        ),
        migrations.AlterField(
            model_name='news',
            name='category',
            field=models.ForeignKey(help_text='Loại bài viết.', on_delete=django.db.models.deletion.CASCADE, to='index.Category'),
        ),
        migrations.AlterField(
            model_name='news',
            name='content',
            field=ckeditor_uploader.fields.RichTextUploadingField(help_text='Nội dung của bài viết'),
        ),
        migrations.AlterField(
            model_name='news',
            name='description',
            field=ckeditor.fields.RichTextField(help_text='Mô tả ngắn', null=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='is_active',
            field=models.BooleanField(default=True, help_text='Trạng thái.'),
        ),
        migrations.AlterField(
            model_name='news',
            name='is_news',
            field=models.BooleanField(default=True, help_text='có phải tin tức hay không.'),
        ),
        migrations.AlterField(
            model_name='news',
            name='tags',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='news',
            name='title',
            field=models.CharField(help_text='Tiêu đề.', max_length=200),
        ),
        migrations.AlterField(
            model_name='slide',
            name='description',
            field=ckeditor.fields.RichTextField(help_text='Mô tả', null=True),
        ),
        migrations.AlterField(
            model_name='slide',
            name='image_slide',
            field=models.ImageField(help_text='Vị trí', null=True, upload_to='image/'),
        ),
        migrations.AlterField(
            model_name='slide',
            name='is_active',
            field=models.BooleanField(default=False, help_text='Trạng thái'),
        ),
    ]
