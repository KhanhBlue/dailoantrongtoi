from django.contrib import admin
from index.models import Category, insertScript
from index.models import News, Slide, logoOrBanner

admin.site.site_header = "Trang quản lý Tindailoan.net"
admin.site.site_title = "Tindailoan.net management"
admin.site.index_title =  'Tin Đài Loan'

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'position_menu', 'author', 'create_at']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(Category, CategoryAdmin)


class NewsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title','category', 'author', 'create_at']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(News, NewsAdmin)

class SlideAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'position_slide']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(Slide, SlideAdmin)

class logoOrBannerAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'is_active']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(logoOrBanner, logoOrBannerAdmin)

class insertScriptAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'is_active']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(insertScript, insertScriptAdmin)
