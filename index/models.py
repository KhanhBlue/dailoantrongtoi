from django.db import models
from django.contrib import admin
import uuid
from django import forms
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.text import slugify
import itertools


class Category(models.Model):
    class Meta:
        verbose_name_plural = "Chuyên mục(Category)"
    # lấy danh mục tin tức cũng là menu
    title = models.CharField(max_length=200, help_text="Tiêu đề")
    description = models.CharField(max_length=200, help_text="Mô tả của tiêu đề")
    create_at = models.DateTimeField(auto_now_add = True, help_text="Ngày tạo")
    author = models.CharField(max_length=200, help_text="Tác giả", blank=True)
    title_url = models.CharField(max_length=200,null=True, default='', blank=True)
    avata = models.ImageField(upload_to='image/', null=True, help_text="Ảnh đại diện.", blank=True)
    position_menu = models.IntegerField(null=True, default=0, help_text="Vị trí menu hiển thị.")
    menu_parent = models.IntegerField(null=True, default=0, help_text="VD: Nếu là 'ĐƠN HÀNG ĐÀI TRUNG' sẽ có menu parent là 'XKLĐ_ DU HỌC'")
    is_menu_parent = models.IntegerField(null=True, default=0, help_text="VD: Nếu là 'ĐƠN HÀNG ĐÀI TRUNG' sẽ điền là 0 - nếu là 'XKLĐ_ DU HỌC' điền 1 => Không có menu con sẽ điền là 0, có điền là 1")
    is_category = models.BooleanField(default=False, help_text="Nếu là trang có nhiều bài viết vd: TIN ĐÀI LOAN không tích. Nếu là trang Liên Hệ sẽ tích")
    is_active = models.BooleanField(default=True, help_text="Còn hoạt động hay không.")
    slug = models.CharField(max_length=500, blank=True)

    def _generate_slug(self):
        value = convertVietnamesToEnglish(self.title)
        slug_candidate = slug_original = slugify(value, allow_unicode=True)
        for i in itertools.count(1):
            if not News.objects.filter(slug=slug_candidate).exists():
                break
            slug_candidate = '{}-{}'.format(slug_original, i)

        self.slug = slug_candidate

    def save(self, *args, **kwargs):
        # if not self.pk:
        self._generate_slug()

        super().save(*args, **kwargs)


    def __str__(self):
        return self.title

import re

def convertVietnamesToEnglish(s):
    s = s.lower()
    s = re.sub('[áàảãạăắằẳẵặâấầẩẫậ]', 'a', s)
    s = re.sub('[éèẻẽẹêếềểễệ]', 'e', s)
    s = re.sub('[óòỏõọôốồổỗộơớờởỡợ]', 'o', s)
    s = re.sub('[íìỉĩị]', 'i', s)
    s = re.sub('[úùủũụưứừửữự]', 'u', s)
    s = re.sub('[ýỳỷỹỵ]', 'y', s)
    s = re.sub('đ', 'd', s)
    return s

class News(models.Model):
    class Meta:
        verbose_name_plural = "Bài viết(News)"
    # Các bài viết sẽ được chia với từng danh mục
    title = models.CharField(max_length=200, help_text="Tiêu đề.")
    create_at = models.DateTimeField(auto_now_add = True)
    author = models.CharField(max_length=200, help_text="Người tạo", default="Tin Đài Loan")
    description = RichTextField(null=True, help_text="Mô tả ngắn")
    content = RichTextUploadingField(help_text="Nội dung của bài viết")
    slug = models.CharField(max_length=200, blank=True)
    tags = models.CharField(max_length=200, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, help_text="Loại bài viết.")
    avata = models.ImageField(upload_to='image/', height_field=None, width_field=None, max_length=100, null=True, help_text="Ảnh đại diện")
    views_number = models.IntegerField(null=True, default=0)
    is_news = models.BooleanField(default=True, help_text="có phải tin tức hay không.")
    is_active = models.BooleanField(default=True, help_text="Trạng thái.")

    def _generate_slug(self):
        value = convertVietnamesToEnglish(self.title)
        slug_candidate = slug_original = slugify(value, allow_unicode=True)
        for i in itertools.count(1):
            if not News.objects.filter(slug=slug_candidate).exists():
                break
            slug_candidate = '{}-{}'.format(slug_original, i)

        self.slug = slug_candidate

    def save(self, *args, **kwargs):
        # if not self.pk:
        try:
            if not News.objects.get(id=self.id).views_number < self.views_number:
                self._generate_slug()
        except:
            pass
        super().save(*args, **kwargs)

class Slide(models.Model):
    class Meta:
        verbose_name_plural = "Slide(5 ảnh động)"
    title = models.CharField(max_length=200)
    description = RichTextField(null=True, help_text="Mô tả")
    image_slide = models.ImageField(upload_to='image/', height_field=None, width_field=None, max_length=100, null=True, help_text="Vị trí")
    position_slide = models.IntegerField(null=True, default=0)
    Author = models.CharField(max_length=200, null=True)
    is_active = models.BooleanField(default=False, help_text="Trạng thái")

    def __str__(self):
        return self.title


IMAGE_CHOICES = (
    ("logo", "logo"),
    ("banertop", "banertop"),
    ("logohead", "logohead"),
    ("orther", "orther")
)

POSITION_CHOICES = (
    ("header", "header"),
    ("footer", "footer")
)

class logoOrBanner(models.Model):
    class Meta:
        verbose_name_plural = "Logo và Banner"
    title = models.CharField(max_length=200)
    create_at = models.DateTimeField(auto_now_add = True)
    description = RichTextField(null=True)
    image_logoOrBanner = models.ImageField(upload_to='image/', height_field=None, width_field=None, max_length=100, null=True)
    logoOrBanner = models.CharField(max_length = 20, choices = IMAGE_CHOICES, default = 'logo') 
    Author = models.CharField(max_length=200, null=True)
    is_active = models.BooleanField(default=False)
    
    def __str__(self):
        return self.title

class scriptFacebook(models.Model):
    title = models.CharField(max_length=200)
    create_at = models.DateTimeField(auto_now_add=True)
    description = RichTextField(null=True)
    scriptFacebook = models.CharField(max_length=10000, null=True)
    Author = models.CharField(max_length=200, null=True)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.title        

class insertScript(models.Model):
    class Meta:
        verbose_name_plural = "Nhúng mã code(header hoặc footer)"

    title = models.CharField(max_length=200, help_text="Tiêu đề")
    create_at = models.DateTimeField(auto_now_add=True, help_text="Ngày tạo")
    description = RichTextField(null=True, help_text="Mô tả")
    script = models.CharField(max_length=10000, null=False,  help_text="Đoạn mã cần chèn")
    Author = models.CharField(max_length=200, null=True, help_text="Tác giả", default="admin")
    position = models.CharField(max_length = 20, choices = POSITION_CHOICES, default = 'position', help_text="Vị trí muốn chèn") 
    is_active = models.BooleanField(default=True, help_text="Trạng thái")

    def __str__(self):
        return self.title

